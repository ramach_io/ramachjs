'use strict'

const curryAnything = (fn, limit) => {
  function curried(lmt, ...allArgs) {
    return lmt && allArgs.length >= lmt ? (
      fn(...allArgs)
    ) : (...args) => curried(
      args.length ? lmt : allArgs.length,
      ...allArgs,
      ...args
    )
  }
  return curried(limit)
}

function leftPad(str, len = 2, char = '0') {
  return `${str}`.length < len ? (
    leftPad(char + `${str}`, len, char)
  ) : str
}

const logTap = log => (
  (noun, adjective = '', verb = () => '') => {
    log(adjective, noun, verb(noun))
    return noun
  }
)

const template = (strings, ...args) => strings.reduce(
	(acc, str, i) => {
  	return `${acc}${str}${args[i] || ''}`
  },
'')

module.exports = {
  curryAnything,
  leftPad,
  logTap,
  template,
}
