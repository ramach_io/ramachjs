'use strict'

const {
  curryAnything,
  leftPad,
  logTap,
} = require('./ramach')

module.exports = {
  curryAnything,
  leftPad,
  logTap: logTap(console.log),
}
