'use strict'

const {
  curryAnything,
  leftPad,
  logTap,
  template,
} = require('./ramach')

describe(`UNIT ${__dirname}`, () => {
  describe('curryAnything()', () => {
    const summation = (...args) => (
      args.reduce((acc, arg) => (acc + arg), 0)
    )

    test('given no limit', () => {
      expect(curryAnything(summation)(2, 3, 4)()).toEqual(9)
      expect(curryAnything(summation)(2, 3)(4)()).toEqual(9)
      expect(curryAnything(summation)(2)(3, 4)()).toEqual(9)
      expect(curryAnything(summation)(2)(3)(4)()).toEqual(9)
    })

    test('given a limit', () => {
      expect(curryAnything(summation, 3)(2, 3)(4)).toEqual(9)
      expect(curryAnything(summation, 3)(2)(3, 4)).toEqual(9)
      expect(curryAnything(summation, 3)(2)(3)(4)).toEqual(9)
      expect(curryAnything(summation, 3)(2)()).toEqual(2)
    })
  })

  describe(`leftPad`, () => {
    test('given a string', () => {
      const dollars = 15
      const cents = 4

      expect(`$${dollars}.${leftPad(cents)}`).toEqual('$15.04')
    })

    test('given a string and length', () => {
      const agents = [
        'Silva',
        'Donne',
        'Fairbanks',
        'Walters',
        'Flemmings',
        'Thomas',
        'Trevelyan',
        'Bond',
        'Timothy',
        'Smith'
      ]

      const codeName = leftPad(agents.findIndex(
        agent => agent === 'Bond'
      ), 3)

      expect(codeName).toEqual('007')
    })

    test('given a string, length, and char', () => {
      expect(leftPad('', 3, '.')).toEqual('...')
    })
  })

  describe('logTap()', () => {
    let logged
    const log = (adj, noun, adv) => {
      logged = `${adj && adj + ' '}${noun}${adv && ' ' + adv}`
    }

    test('given no label or function', () => {
      const returned = logTap(log)(5)
      
      expect(logged).toEqual('5')
      expect(returned).toEqual(5)
    })

    test('given a label and no function', () => {
      const returned = logTap(log)(5, 'NUMBER')

      expect(logged).toEqual('NUMBER 5')
      expect(returned).toEqual(5)
    })

    test('given a label and a function', () => {
      const times = y => x => x * y
      const returned = logTap(log)(5, 'NUMBERS', times(4))
      
      expect(logged).toEqual('NUMBERS 5 20')
      expect(returned).toEqual(5)
    })
  })

  describe('template()', () => {
    const tag = (...args) => template(...args)
    const a = 'Silly'
    const b = 'silly'
    
    expect(tag`Here is a ${a} string for a ${b} goose.`)
      .toEqual('Here is a Silly string for a silly goose.')
    expect(tag`${a} string for a ${b} goose.`)
      .toEqual('Silly string for a silly goose.')
    expect(tag`${a} string for a ${b}`)
      .toEqual('Silly string for a silly')
  })
})
