# ramachjs

## curryAnything

Partially apply any function's parameters to create an arbitrary number of intermediate curried functions.

The underlying function will execute when the total number of parameters is equal or greater than the given `limit` (if defined) or the curried function is called no parameters, whichever occurs first.

Parameters:

* `fn`: the function to be curried
* `limit` (optional): the total number of args to accept before executing `fn`

```
// Examples:

const summation = (...args) => (
  args.reduce((acc, arg) => (acc + arg), 0)
)

// with no limit
curryAnything(summation)(2, 3, 4)()   // -> 9
curryAnything(summation)(2, 3)(4)()   // -> 9
curryAnything(summation)(2)(3, 4)()   // -> 9
curryAnything(summation)(2)(3)(4)()   // -> 9

// with limit of 3
curryAnything(summation, 3)(2, 3)(4)  // -> 9
curryAnything(summation, 3)(2)(3, 4)  // -> 9
curryAnything(summation, 3)(2)(3)(4)  // -> 9
curryAnything(summation, 3)(2)()      // -> 2
```

## leftPad

Pads the left side of a string with a given number of occurrences of a given character.

Parameters:

* `str`: the string to be padded
* `len` (optional): the total desired string length (default = 2)
* `char` (optional): the character to pad out the string with (default = '0')

```
// Basic usage:

const dollars = 15
const cents = 4

const total = `$${dollars}.${leftPad(cents)}`

// returns '$15.04'
```

```
// Given a set number of digits:


const agents = [
  'Silva',
  'Donne',
  'Fairbanks',
  'Walters',
  'Flemmings',
  'Thomas',
  'Trevelyan',
  'Bond',
  'Timothy',
  'Smith'
]

const codeName = leftPad(agents.findIndex(
  agent => agent === 'Bond'
), 3)

// returns '007'
```

```
// Given a number of digits and a char:

const elipsis = leftPad('', 3, '.')

// returns '...'
```

## logTap

Simple utility that logs a thing and then returns it.

Parameters:

* `noun`: the value to be printed and returned
* `adjective` (optional): a label to be printed ahead of the noun
* `verb` (optional): a function executed on `noun` whose return value will be printed after

```
// Basic usage:

const logged = logTap(5)

// prints 5 to console
// returns 5
```
```
// Use with label:

const logged = logTap(5, 'NUMBER')

// prints 'NUMBER 5' to console
// returns 5
```
```
// Use with label and function:

const logged = logTap(5, 'NUMBERS', x => x * 4)

// prints 'NUMBERS 5 20' to console
// returns 5
```

## template

Utility to simplify tagged templates.

Parameters:

* `...args`

```
// Usage:

const a = 'Silly'
const b = 'silly'
const myTag = (...args) = template(...args)

const computed = myTag`${a} string for a ${b} goose.`

// returns 'Silly string for a silly goose.'
